import csv
import os

filename = 'hogwarts.csv'

def house_selection(filename, student_name, age, school_year):
    test = []
    with open(filename, 'r') as read:
        for row in csv.reader(read):
            test.append(row)
            if row[0] == str(student_name):
                return 'Nome de usuário já extiste'
    
    with open(filename, 'a+') as f:
        
        fieldnames = ['student_name', 'age', 'house_name', 'school_year']
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        
        if os.stat(filename).st_size == 0:
            writer.writeheader()

        if student_name == 'Harry Potter':
            house_name = 'Gryffindor'
        else:
            house_name = 'Slytherin'

        writer.writerow({
            'student_name': student_name,
            'age': age,
            'house_name': house_name,
            'school_year': school_year
        })
    

def list_all(filename):
    students = ''
    with open(filename, 'r') as list_csv:
        for row in csv.DictReader(list_csv):
            students += str(row)

        return students