from flask import Flask, request, request
from app.handlers import *
import csv
import os

filename = 'hogwarts.csv'

app = Flask(__name__)
    
@app.route("/selection", methods=["POST"])
def selection():
    student_name = request.json.get('student_name')
    age = request.json.get('age')
    school_year = 1
    
    if student_name == 'Harry Potter':
        house_name = 'Gryffindor'
    else:
        house_name = 'Slytherin'

    house_selection(filename, student_name, age, school_year)
    
    return dict(student_name=student_name, age=age, house_name=house_name, school_year=school_year)

@app.route("/students", methods=["GET"])
def list_students():
    result = list_all(filename)
    
    return result