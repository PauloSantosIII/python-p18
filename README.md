# Daily Practice

#### Hoje você utilizará os conhecimentos adquiridos com o material e a demo, para criar uma aplicação mínima, a qual os dados possam ser gravados em um csv. A aplicação deverá ser capaz de cadastrar e listar os novos estudantes de Hogwarts

# A aplicação deverá seguir a seguinte estrutura:

        ├── app
        │   ├── handlers.py
        │   ├── __init__.py
        ├── .git
        ├── README.md
        ├── requirements.txt
        └── venv
 

## ROTAS
### Selecionar um Novo Estudante para uma Casa de Hogwarts: ⚡👓

    @app.route("/selection", methods=["POST"])
    def selection():
        pass

* Procedimento:
    * Receber os dados da requisição e utilizar a função house_selection dentro do arquivo handler.py para selecionar o novo estudante para uma casa de Hogwarts

* Response:
    * O nome do estudante e a casa de Hogwarts que foi selecionado 

## EXEMPLOS:

1. Exemplo do Harry Potter
2. Exemplo do Draco Malfoy
 
### Listar Estudantes da Escola de Bruxaria Hogwarts

    @app.route("/students", methods=["GET"])
    def list_students():
        pass
* Utilizar o método list_all do arquivo handlers.py para obter os estudantes de Hogwarts
* Caso não exista nenhum estudante em Hogwarts, retornar uma lista vazia

## EXEMPLOS:

1. Exemplo de Listagem
## Responsabilidades
* hogwarts.csv 📚🤔
    * Local de armazenamento de cada um dos estudantes de Hogwarts
    * HEADER: student_name,age,house_name,school_year 
* __init__.py
    * Deve conter a seguinte função:
        * create_app()
            * Deve criar a instância app da classe Flask
            * Inserir as rotas da aplicação
            * Retornar a instância de Flask
* handlers.py
    * Deve conter as seguintes funções:
        * house_selection(filename, student_name, age, school_year)
            * Selecionar de forma aleatória uma das casas a seguir: Gryffindor, Hufflepuff, Ravenclaw, and Slytherin.
            * Inserir um novo estudante de Hogwarts dentro do banco de dados
            * OBS: O atributo school_year de um estudante deve ser sempre 1
        * list_all(filename)
            * Deve retornar a lista de todos os estudantes de Hogwarts em forma de dicionário